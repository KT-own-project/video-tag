// Modules to control application life and create native browser window
const electron = require("electron")
const { app, Browser, screen, ipcMain, ipcRenderer, BrowserWindow } = require("electron") // input particular elements from electron
const path = require('path')
const jsonfile = require('jsonfile');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');//  meaning in-memory

async function createWindow() {
	const mainScreen = screen.getPrimaryDisplay();
	const dimensions = mainScreen.size;

	mainWindow = new BrowserWindow({
		width: 600,
		height: 500,
		hasShadow: true,
		webPreferences: {
			nodeIntegration: true
		}
	})

	// and load the index.html of the app.
	mainWindow.loadFile('./html/index.html')
	// mainWindow.webContents.openDevTools()
	const reset = [];
	await jsonfile.writeFile('./data/tags.json', reset);

};

app.on('ready', createWindow);

ipcMain.on('resize', function (e, x, y) {
	mainWindow.setSize(x, y);
});


// Quit when all windows are closed.
app.on('window-all-closed', async function () {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') app.quit()
	db.close();
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
