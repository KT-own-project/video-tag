const electron = require('electron');
const jsonfile = require('jsonfile');
const checkFolder = require('../checkFolder');
const checkVideoFile = require('../checkVideoFile');
const uploadVideo = require('../upload');
const path = require('path');
const OBSWebSocket = require('obs-websocket-js');
const obs = new OBSWebSocket();
const fs = require('fs');

let description = '';

let { ipcRenderer } = electron;
ipcRenderer.send('resize', 1100, 100);

function getTime(ms) {
    var seconds = (ms / 1000).toFixed(0);
    var minutes = Math.floor(seconds / 60);
    var hours = "";
    if (minutes > 59) {
        hours = Math.floor(minutes / 60);
        hours = (hours >= 10) ? hours : "0" + hours;
        minutes = minutes - (hours * 60);
        minutes = (minutes >= 10) ? minutes : "0" + minutes;
    }

    seconds = Math.floor(seconds % 60);
    seconds = (seconds >= 10) ? seconds : "0" + seconds;
    if (hours != "") {
        return hours + ":" + minutes + ":" + seconds;
    }
    return minutes + ":" + seconds;
}

function connectOBS() {
    obs.connect({
        address: 'localhost:4444',
        password: '123456'
    });
}

connectOBS();

let count = true;
let startTime;
const recording = document.querySelector('#recording');
recording.addEventListener('click', async function () {
    const videoPath = await jsonfile.readFile('./data/videoPath.json');
    const videoTitle = await jsonfile.readFile('./data/videoTitle.json');

    if (count) {
        await checkFolder(videoPath);
        await obs.send('StartRecording');
        startTime = new Date();
        console.log(startTime);
        const tags = await jsonfile.readFile('./data/tags.json');
        for (tag of tags) {
            tagData.innerHTML += `<div class="single-tag">
            <button class="tagButton ${tag}">${tag}</button>     
            </div>`

            const tagButton = document.querySelectorAll('.tagButton');
            for (let i = 0; i < tagButton.length; i++) {
                tagButton[i].onclick = async function () {
                    let clickTime = new Date();
                    let time = clickTime - startTime;
                    const tagTime = getTime(time);
                    console.log(tagTime);
                    tagButton[i].insertAdjacentHTML("afterend", `<h5 class="tag-time">${tagTime}</h5>`)
                    const tag = (tags[i]);
                    description += `${tagTime} ${tag} \n`;
                    console.log(description);
                }
            }
        }
        count = false;
        recording.innerHTML = "Stop";
    } else {
        await obs.send('StopRecording');
        const videoFile = await checkVideoFile(videoPath);
        console.log(videoFile);
        await uploadVideo(videoTitle, description, path.join(videoPath, videoFile));
        count = true;
        recording.innerHTML = "Start";
        console.log(videoTitle);
        console.log(videoPath);
    }
})

// let tagCount = 0;
const tagData = document.querySelector('.tag-box');
const addTag = document.querySelector('#add-tag');
const tagValue = document.querySelector('#tag-value');
addTag.addEventListener('click', async function () {

    let tagName = tagValue.value;
    if (tagValue.value != "") {
        tagData.innerHTML += `<div class="single-tag">
        <button class="tagButton ${tagName}">${tagName}</button>      
        </div>`
    }
    const tagButton = document.querySelectorAll('.tagButton');
    for (let i = 0; i < tagButton.length; i++) {
        tagButton[i].onclick = async function () {
            let clickTime = new Date();
            let time = clickTime - startTime;
            const tagTime = getTime(time);
            console.log(tagTime);
            tagButton[i].insertAdjacentHTML("afterend", `<h5 class="tag-time">${tagTime}</h5>`)
            description += `${tagTime} ${tagName} \n`;
            console.log(description);
        }
    }
    tagValue.value = '';
})






