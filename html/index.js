const fs = require('fs');
const jsonfile = require('jsonfile');

window.onload = async function () {
    const videoPath = await fs.promises.readFile('./data/videoPath.txt');
    pathValue.value = videoPath;
}

const videoTitleValue = document.querySelector('#videoTitle');
const pathValue = document.querySelector('#videoPath');
const btn = document.querySelector('#openTagging');

btn.addEventListener('click', async function () {

    const videoTitle = videoTitleValue.value;
    const videoPath = pathValue.value;
    if (videoPath != "" && videoTitle != "") {
        await jsonfile.writeFile('./data/videoPath.json',videoPath);
        await jsonfile.writeFile('./data/videoTitle.json', videoTitle);
        await fs.promises.writeFile('./data/videoPath.txt', videoPath);
        await window.location.replace("tagging.html");
    }else{
        alert('Please enter the video title!');
    }
})

const exec = require('child_process').exec;

function execute(command, callback) {
    exec(command, {
        shell: true
    }, (error, stdout, stderr) => {
        callback(stdout);
    });
};

let plat = process.platform
if (plat === 'darwin') {
    execute('open -a OBS --args --minimize-to-tray', (output) => {
        console.log(output);
    });
} else if (plat === 'win32') {
    console.log('It is a window')
} else if (plat === 'linux') {
    console.log('It is a linux')
} else {
    console.log('Can not find')
}

const tagData = document.querySelector('.tag-box');
const addTag = document.querySelector('#add-tag');
const tagValue = document.querySelector('#tag-value');

addTag.addEventListener('click', async function () {
    let tagName = tagValue.value;
    if (tagValue.value != "") {
        tagData.innerHTML += `<div class="single-tag">
        <button id="tags" type="button" class="btn btn-outline-info" ${tagName}">${tagName}</button>
        </div>`
        const tags = await jsonfile.readFile('./data/tags.json');
        tags.push(
            tagName
        )
        await jsonfile.writeFile('./data/tags.json', tags, { spaces: 4 });
    }
    tagValue.value = '';
})