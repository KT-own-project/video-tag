const fs = require('fs');
const jsonfile = require('jsonfile');

module.exports = async function checkVideoFile(videoPath) {
    const filesAfterRecording = await fs.promises.readdir(videoPath);
    const files = await jsonfile.readFile('./checkFile.json');
    files.push(
        filesAfterRecording
    )
    await jsonfile.writeFile('checkFile.json', files, { spaces: 4 });

    const totalFiles = await jsonfile.readFile('./checkFile.json');

    const videoFile = await function () {
        for (let file of totalFiles[1]) {
            if (!totalFiles[0].includes(file)) {
                return (file);
            }
        }
    }
    return(videoFile());
}