const youtube = require('youtube-api');
const fs = require('fs');
const lien = require('lien');
const open = require('open');
const logger = require('bug-killer');
const jsonfile = require('jsonfile');

module.exports = async function uploadVideo(title, description, folderPath) {

    const CREDENTIALS = await jsonfile.readFile('./credentials.json');

    let server = new lien({
        host: "localhost"
        , port: 5000
    });
    let oauth = youtube.authenticate({
        type: "oauth"
        , client_id: CREDENTIALS.web.client_id
        , client_secret: CREDENTIALS.web.client_secret
        , redirect_url: CREDENTIALS.web.redirect_uris[0]
    });

    open(oauth.generateAuthUrl({
        access_type: "offline"
        , scope: ["https://www.googleapis.com/auth/youtube.upload"]
    }));

    // Handle oauth2 callback
    server.addPage("/oauth2callback", (lien) => {
        oauth.getToken(lien.query.code, (err, tokens) => {

            if (err) {
                lien.lien(err, 400);
                return (err);
            }

            oauth.setCredentials(tokens);

            lien.end("The video is uploading.");

            const req = youtube.videos.insert({
                resource: {
                    snippet: {
                        title: title //input video title here
                        , description: description //input description here
                    }
                    , status: {
                        privacyStatus: "private"
                    }
                }
                , part: "snippet,status"
                , media: {
                    body: fs.createReadStream(folderPath) //the video will be upload
                }
            }, (err, data) => {
                console.log("Done.");
                process.exit();
            });

            setInterval(function () {
                logger.log(`${(req.req.connection._bytesDispatched)} bytes uploaded.`);
            }, 250);
        });
    });

}
