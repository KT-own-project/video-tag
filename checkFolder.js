const fs = require('fs');
const jsonfile = require('jsonfile');

module.exports = async function checkFolder(videoPath) {
    const files = await fs.promises.readdir(videoPath);
    const checkFile = [];
    checkFile.push(
        files
    )
    await jsonfile.writeFile('checkFile.json', checkFile, { spaces: 4 });
}
